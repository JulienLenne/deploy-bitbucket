module.exports = {
    email: {
        sendOnSuccess: false,
        sendOnError: false,
        to: "contact.lenne@gmail.com",
        from: "node-deploy-hook",
        subjectOnSuccess: "Successfully updated repository",
        subjectOnError: "Failed to update respository",
        transport: {
            service: "Gmail",
            auth: {
                user: "contact.lenne@gmail.com",
                pass: ""
            }
        }
    },
    port: 8888,
    serverRoot: '/var/www/'

};
